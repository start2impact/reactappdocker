import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


function PageA(){
  return <div className='page'>I am page A</div>
}

function PageB(){
  return <div className='page'>I am page B</div>
}


function App() {
  return (
    <Router>
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          <div className='menu'>
            <Link to="/">Home</Link>
            <Link to="/pageA">PageA</Link>            
            <Link to="/pageB">PageB</Link>
          </div>
        </a>

        <Switch>
          <Route path="/pageA">
            <PageA />
          </Route>
          <Route path="/pageB">
            <PageB />
          </Route>
        </Switch>
      </header>      
    </div>
    </Router>
  );
}

export default App;
